# Docker Compose. Пример приложения-имитации микросервисной архитектуры с RabbitMQ, Producer и Consumer

Этот проект представляет собой пример приложения, включающего RabbitMQ (брокер сообщений), Producer (генератор сообщений) и Consumer (обработчик сообщений).
Приложение состоит из трех контейнеров и запускается в соответствии с параметрами в docker-compose.yml с помощью `Docker Compose plugin`.

Producer и Consumer подключаются к брокеру собщений RabbitMQ в сответствии с параметрами указанными в переменных окружения.

Producer создает сообщения формата:

```
[10] You need to process this in: 3 seconds
```
с периодичностью, указанной в перменной окружения `MESSAGE_DELAY`. В сообщении указанно колличество секунд, которое Consumer должен обрабатывать
указанное сообщение. Значения изменяются в пределах от 1 до 7 секунд.
При этом в логах Producer отражается отаправка сообщения в формате:

```
2024-03-01 08:52:40.340455+0000 [info] Sending message #11
```

Consumer забирает сообщения из очереди и имитирует их обработку ожиданием в течение указанного количества секунд.
В процессе работы, он генерирует лог-сообщения следующего формата:

```
2024-03-01 08:52:40.353769+0000 [info] Received message: [11] You need to process this in: 6 seconds
2024-03-01 08:52:40.353846+0000 [info] Processing...
2024-03-01 08:52:46.372671+0000 [info] Ready! Wating for the next task
```

По id сообщений, указанному в формате `[11]` можно определить, успевает ли Consumer обрабатывать сообщения при текущем периоде отправки,
указанном в `MESSAGE_DELAY`

Также для визуализации процесса, доступен RabbitMQ Management Interface на порту 15672

## Зависимости

- [Docker](https://docs.docker.com/engine/install/)
- [Docker Compose plugin](https://docs.docker.com/compose/install/linux/#install-using-the-repository)

## Настройки

Настройки работы приложения производятся через переменные окружения. Переменные представлены со значениями по умолчанию:

| **Название переменной** | **Значение по умолчанию** | **Описание**                                                 |
|-------------------------|---------------------------|--------------------------------------------------------------|
| `RABBITMQ_HOST`         | rabbitmq                  | Hostname или адрес RabbitMQ                                  |
| `RABBITMQ_PORT`         | 5672                      | Порт для сообщений RabbitMQ                                  |
| `RABBITMQ_VHOST`        | /                         | Навание виртуального хоста сообщений RabbitMQ                |
| `RABBITMQ_QUEUE_NAME`   | queue                     | Навание очереди сообщений внутри виртуального хоста RabbitMQ |
| `RABBITMQ_DEFAULT_USER` | rmuser                    | Пользователь для подключения к RabbitMQ                      |
| `RABBITMQ_DEFAULT_PASS` | rmpassword                | Пароль для подключения к RabbitMQ                            |
| `MESSAGE_DELAY`         | 5                         | Таймаут задержки в секундах перед отправкой нового сообщения от producer в очередь|


## Запуск приложения

1. Установите Docker и Docker Compose, если они еще не установлены.
2. Склонируйте репозиторий: `git clone <repository-url>`
3. Перейдите в каталог с проектом: `cd <project-directory>`
4. Запустите приложение в фоновом режиме с помощью Docker Compose:

```bash
docker-compose up -d
```

После выполнения этих шагов, приложение будет развернуто, и вы сможете взаимодействовать с RabbitMQ и наблюдать за генерацией и обработкой сообщений.

```bash
docker logs consumer 
docker logs producer
docker logs rabbitmq-server
```

## Структура проекта

- `rabbitmq/`: Каталог с Dockerfile для сборки образа RabbitMQ.
- `producer/`: Каталог с Dockerfile и исходным кодом для сборки образа Producer.
- `consumer/`: Каталог с Dockerfile и исходным кодом для сборки образа Consumer.
- `credentials.env`: Файл с пользователем и паролем для RabbitMQ.
- `connection_params.env`: Файл с параметрами соединения с RabbitMQ для Producer и Consumer.

## Порты и адреса

RabbitMQ Management Interface: [http://localhost:15672](http://localhost:15672)

Логин по умолчанию: **rmuser**

Пароль по умолчанию: **rmpassword**

## Остановка приложения

Чтобы остановить приложение, выполните команду:

```bash
docker-compose down
```

## Примечания
- Перезапуск контейнеров происходит автоматически в случае завершения.
- Healthcheck настроен для проверки доступности RabbitMQ через HTTP запрос к Management Interface.
- Проект использует собственную сеть rabbitnetwork для взаимодействия контейнеров.

## Практическое задание №3. Docker Compose

1. Клонируйте репозиторий, с примером docker-compose.yml, командой:

```bash
git clone https://gitlab.com/devdsk/docker-lesson/docker-compose.git
```

2. Перейдите в директорию docker-compose и запустите проект командой:

```bash
docker compose up -d
```
3. Проверьте работоспособность RabbitMQ Management Interface по адресу: http://localhost:15672

4. Посмотрите логи каждого сервиса, отработайте команды docker compose.

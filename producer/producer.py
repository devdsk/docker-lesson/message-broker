import pika
import time
import random
import os
import sys
from datetime import datetime, timezone


def init_connection_params(host: str, port: str, virtualhost: str, username: str, password: str):
    connection_parameters = pika.ConnectionParameters(
        host, port, virtualhost, pika.PlainCredentials(username, password))
    return connection_parameters


def send_message_rabbitmq(parameters, queue_type, message="Hello World!"):
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue=queue_type, durable=True)
    channel.basic_publish(exchange='',
                          routing_key=queue_type,
                          body=message,
                          properties=pika.BasicProperties(delivery_mode=2,))
    connection.close()


def write_log(message: str):
    current_utc_time = datetime.utcnow().replace(tzinfo=timezone.utc)
    formatted_time = current_utc_time.strftime('%Y-%m-%d %H:%M:%S.%f%z')
    sys.stdout.write(f"{formatted_time} [info] {message}\n")


if __name__ == "__main__":
    rabbitmq_username = os.environ.get("RABBITMQ_DEFAULT_USER")
    rabbitmq_password = os.environ.get("RABBITMQ_DEFAULT_PASS")
    produce_delay = os.environ.get("MESSAGE_DELAY")
    rabbitmq_host = os.environ.get("RABBITMQ_HOST")
    rabbitmq_port = os.environ.get("RABBITMQ_PORT")
    rabbitmq_virtualhost = os.environ.get("RABBITMQ_VHOST")
    rabbitmq_queue = os.environ.get("RABBITMQ_QUEUE_NAME")

    parameters = init_connection_params(
        rabbitmq_host, rabbitmq_port, rabbitmq_virtualhost, rabbitmq_username, rabbitmq_password)
    id = 0
    while True:
        message = f"[{id}] You need to process this in: {random.randint(1,7)} seconds"
        write_log(f"Sending message #{id}")
        send_message_rabbitmq(parameters, rabbitmq_queue, message)
        time.sleep(int(produce_delay))
        id += 1

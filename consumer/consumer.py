import pika
import time
import sys
import os
import re
from datetime import datetime, timezone


def init_connection_params(host: str, port: str, virtualhost: str, username: str, password: str):
    connection_parameters = pika.ConnectionParameters(
        host, port, virtualhost, pika.PlainCredentials(username, password))
    return connection_parameters


def write_log(message: str):
    current_utc_time = datetime.utcnow().replace(tzinfo=timezone.utc)
    formatted_time = current_utc_time.strftime('%Y-%m-%d %H:%M:%S.%f%z')
    sys.stdout.write(f"{formatted_time} [info] {message}\n")


def callback(channel, method, properties, body):
    recv_message = str(body, 'utf-8')
    write_log(f"Received message: {recv_message}")
    pattern = r'in: (\d+) seconds'
    match = re.search(pattern, recv_message)
    if match:
        write_log("Processing...")
        seconds = int(match.group(1))
        time.sleep(seconds)
        write_log("Ready! Wating for the next task")
    else:
        write_log("Message reading error")
        time.sleep(5)
    channel.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == "__main__":
    rabbitmq_username = os.environ.get("RABBITMQ_DEFAULT_USER")
    rabbitmq_password = os.environ.get("RABBITMQ_DEFAULT_PASS")
    produce_delay = os.environ.get("MESSAGE_DELAY")
    rabbitmq_host = os.environ.get("RABBITMQ_HOST")
    rabbitmq_port = os.environ.get("RABBITMQ_PORT")
    rabbitmq_virtualhost = os.environ.get("RABBITMQ_VHOST")
    rabbitmq_queue = os.environ.get("RABBITMQ_QUEUE_NAME")

    parameters = init_connection_params(
        rabbitmq_host, rabbitmq_port, rabbitmq_virtualhost, rabbitmq_username, rabbitmq_password)
    connection = pika.BlockingConnection(parameters)

    write_log(" [*] Waiting for messages. To exit press CTRL+C")

    channel = connection.channel()
    channel.queue_declare(queue=rabbitmq_queue, durable=True)
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(rabbitmq_queue, callback)

    channel.start_consuming()
